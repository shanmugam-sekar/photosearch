# photosearch

fetching photos using flickr api.

# Features:
    
    Users can query photos.
    Unlimited scrolling supported.
    Image caching implemented.
    We can mock implementation of Network Layer.
    
# Responsibilities of classes:

`PhotosSearchController` - UI representation.

`PhotosSearchViewModel`  - Holds data needed for view.

`PhotoSearchDataManager` - Decision making layer. 
                         Decides to fetch data either from network or local storage (Core Data, etc..). 
                         As of now this layer is fetching from network.

`PhotosSearchAPI`        - Communicates with network manager to fetch data from remote. 
                         
`PhotosSearchParser`     - Parses the response data and returns array of "PhotoModel".

`PhotoModel`             - contains properties.

`PhotoSearchNetworkManager` - responsible for making network calls using iOS API "NSURLSession".
                            currently supports DataTask and DownloadTask.
                            
`ImageDownloader` & `SimpleDataCache` - responsible for fetching photos either from cache if exists or network

# Protocols:

- NetworkManager
- DataCache
- PhotoSearchDataSource
- PhotoCellConfiguration
                         

Test cases written for viewmodel and model.




