//
//  PhotoModelTests.swift
//  PhotoSearchTests
//
//  Created by Shanmugam Sekar on 16/11/18.
//  Copyright © 2018 Shanmugam Sekar. All rights reserved.
//

import XCTest
@testable import PhotoSearch

class PhotoModelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testImageUrlGeneration() {
        let model: PhotoModel? = PhotoModel.init(id: "45171350664", owner: "145509995@N05", secret: "d0eb2eb9ff", server: "4816", farm: 5, title: "2018-11-15_07-57-11", isPublic: true, isFriend: false, isFamily: false, url: nil)
        let modelUrl = model!.constructPhotoURL(model: model!)
        let url = URL.init(string: "http://farm5.static.flickr.com/4816/45171350664_d0eb2eb9ff.jpg")
        XCTAssertEqual(url, modelUrl!)
    }
}
