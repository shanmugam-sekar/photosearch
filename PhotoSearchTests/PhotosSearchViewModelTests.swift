
//
//  PhotosSearchViewModel.swift
//  PhotoSearchTests
//
//  Created by Shanmugam Sekar on 15/11/18.
//  Copyright © 2018 Shanmugam Sekar. All rights reserved.
//

import XCTest
@testable import PhotoSearch

class PhotosSearchViewModelTests: XCTestCase {
    
    var viewModel: PhotosSearchViewModel?
    
    override func setUp() {
        super.setUp()
        viewModel = PhotosSearchViewModel()
        var array: [PhotoModel] = []
        let model1: PhotoModel? = PhotoModel.init(id: "45171350664", owner: "145509995@N05", secret: "d0eb2eb9ff", server: "4816", farm: 5, title: "2018-11-15_07-57-11", isPublic: true, isFriend: false, isFamily: false, url: nil)
        let model2: PhotoModel? = PhotoModel.init(id: "451713506644", owner: "145509995@N05", secret: "d0eb2eb9ff", server: "4816", farm: 5, title: "2018-11-15_07-57-11", isPublic: true, isFriend: false, isFamily: false, url: nil)
        array.append(contentsOf: [model1!,model2!])
        viewModel?.photosList = array
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        viewModel = nil
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testNumberOfPhotosCount() {
        var result: Int = 0
        if let count = viewModel?.getPhotosCount() {
            result = count
        }
        XCTAssertTrue(result == 2)
    }
    
    func testGetPhotoModelForInvalidRow() {
        let photoModel = viewModel?.getPhotoModel(at: 3)
        XCTAssertNil(photoModel)
    }
    
    func testGetPhotoModelForValidRow() {
        let photoModel = viewModel?.getPhotoModel(at: 1)
        XCTAssertNotNil(photoModel)
    }        
}
