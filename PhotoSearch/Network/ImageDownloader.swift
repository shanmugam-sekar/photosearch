//
//  ImageDownloader.swift
//  PhotoSearch
//
//  Created by Shanmugam Sekar on 15/11/18.
//  Copyright © 2018 Shanmugam Sekar. All rights reserved.
//

import Foundation
import UIKit

protocol DataCache {
    func cachedData(key: String) -> Data?
    func cacheData(key: String, data: Data)
}

class SimpleDataCache: DataCache {
    
    static let dataCache = SimpleDataCache()
    private var cache: [String: Data] = [:]
    
    func cachedData(key: String) -> Data? {
        return cache[key]
    }
    
    func cacheData(key: String, data: Data) {
        cache[key] = data
    }
}

class ImageDownloader {
    static let sharedInstance = ImageDownloader()
    /* Cache Implementation Can Be Changed */
    var cache: DataCache? = SimpleDataCache()
    /* Network manager Implementation Can Be Changed */
    var networkManager: NetworkManager = PhotoSearchNetworkManager.sharedNetworkManager
    
    func fetch(url: URL, completion: @escaping (UIImage) -> Void) {
        if let cachedData = cache?.cachedData(key: url.absoluteString) , let image = UIImage.init(data: cachedData){
            completion(image)
            return
        }
        networkManager.download(url: url.absoluteString, parameters: nil) { [weak self] (data, error) in
            if let _ = error {
                
            } else if let data = data as? Data, let image = UIImage.init(data: data) {
                self?.cache?.cacheData(key: url.absoluteString, data: data)
                DispatchQueue.main.async {
                    completion(image)
                }                
            }
        }
    }
}
