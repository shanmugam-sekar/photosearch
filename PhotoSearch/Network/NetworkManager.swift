//
//  NetworkManager.swift
//  PhotoSearch
//
//  Created by Shanmugam Sekar on 15/11/18.
//  Copyright © 2018 Shanmugam Sekar. All rights reserved.
//

import Foundation

protocol NetworkManager {
    typealias resultHandler = (_ data: Any?, _ error: NSError?) -> Void
    func GET(url: String, parameters: [String: String]?, completion: @escaping resultHandler)
    func download(url: String, parameters: [String: String]?, completion: @escaping resultHandler)
}

enum REQUESTMETHOD: String {
    case GET = "GET"
}

struct ErrorInfo {
    static let domain = "com.shanmugam.photos"
    static let errorCode = 1000
    static let errorInfo = "Unknown Error"
}

class PhotoSearchNetworkManager: NetworkManager {
    
    static let sharedNetworkManager: PhotoSearchNetworkManager = PhotoSearchNetworkManager()
    private var generalError: NSError {
        let error = NSError(domain: ErrorInfo.domain, code: ErrorInfo.errorCode, userInfo: [NSLocalizedDescriptionKey: ErrorInfo.errorInfo])
        return error
    }
    
    func GET(url: String, parameters: [String : String]?, completion: @escaping NetworkManager.resultHandler) {
        guard let url = URL.init(string: url) else {
            return
        }
        guard let request = configureURLRequest(url: url, method: .GET, parameters: parameters) else {
            return
        }
        URLSession.shared.dataTask(with: request) { [weak self] (data, response, error) in
            if error != nil {
                completion(nil, self?.generalError)
            } else {
                if let data = data {
                    let result = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
                    completion(result,nil)
                }
            }
        }.resume()
    }
    
    func download(url: String, parameters: [String : String]?, completion: @escaping NetworkManager.resultHandler) {
        guard let url = URL.init(string: url) else {
            return
        }
        guard let request = configureURLRequest(url: url, method: .GET, parameters: parameters) else {
            return
        }
        URLSession.shared.downloadTask(with: request) { [weak self] (url, response, error) in
            if error != nil {
                completion(nil, self?.generalError)
            } else {
                if let url = url {
                    let data = try? Data.init(contentsOf: url)
                    completion(data,nil)
                }
            }
        }.resume()
    }
    
    func configureURLRequest(url: URL,method: REQUESTMETHOD,parameters: [String : String]?) -> URLRequest? {
        var queryItems: [URLQueryItem] = []
        if let data = parameters {
            for (key, value) in data {
                let item = URLQueryItem(name: key, value: value)
                queryItems.append(item)
            }
        }
        guard var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
            return nil
        }
        if !queryItems.isEmpty {
            urlComponents.queryItems = queryItems
        }
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = method.rawValue
        request.setValue("application/json", forHTTPHeaderField: "accept")
        return request
    }
    
}
