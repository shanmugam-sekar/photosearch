//
//  PhotosSearchAPI.swift
//  PhotoSearch
//
//  Created by Shanmugam Sekar on 15/11/18.
//  Copyright © 2018 Shanmugam Sekar. All rights reserved.
//

import Foundation

struct APIInfo {
    static let url = "https://api.flickr.com/services/rest/"
    static var apiKey = "3e7cc266ae2b0e0d78e279ce8e361736"
    static let photoSearchMethod = "flickr.photos.search"
    static let jsonResultsFormat = "json"
}

class PhotosSearchAPI {
    static let searchAPI = PhotosSearchAPI()
    /*
     Network Manager Implementation can be changed
     */
    var networkManager: NetworkManager = PhotoSearchNetworkManager.sharedNetworkManager
    
    func fetchPhotosList(of query: String,page: Int,perPageCount: Int, completion: @escaping (([PhotoModel],PhotosListMetaData)?,String?) -> Void) {
        let url = APIInfo.url
        var parameters: [String: String] = [:]
        parameters["api_key"] = APIInfo.apiKey
        parameters["method"] = APIInfo.photoSearchMethod
        parameters["format"] = APIInfo.jsonResultsFormat
        parameters["nojsoncallback"] = 1.description
        parameters["safe_search"] = 1.description
        parameters["text"] = query
        parameters["page"] = page.description
        parameters["per_page"] = perPageCount.description
        networkManager.GET(url: url, parameters: parameters) { (json, error) in
            if let error = error {
                completion(nil,error.localizedDescription)
            } else if let data = json as? [String: Any] {
                let result = PhotosSearchParser.parser.parsePhotosList(data: data)
                completion(result,nil)
            }
        }
    }
}

class PhotosSearchParser {
    
    static let parser = PhotosSearchParser()
    
    func parsePhotosList(data: [String: Any]) -> ([PhotoModel],PhotosListMetaData) {
        var list: [String: Any] = [:]
        var photos: [PhotoModel] = []
        if let data = data["photos"] as? [String: Any] {
            list = data
        }
        let page = list["pages"] as? Int ?? 0
        let count = list["total"] as? String ?? "0"
        let photosListMetaData = PhotosListMetaData.init(pageCount: page, imageCount: count)
        if let data = list["photo"] as? [[String: Any]] {
            photos = data.compactMap({ (param) -> PhotoModel? in
                return PhotoModel.init(json: param)
            })
        }
        return (photos,photosListMetaData)
    }
    
}
