//
//  PhotoCell.swift
//  PhotoSearch
//
//  Created by Shanmugam Sekar on 15/11/18.
//  Copyright © 2018 Shanmugam Sekar. All rights reserved.
//

import UIKit

protocol PhotoCellConfiguration {
    func configureCell(with url: URL?)
}

class PhotoCell: UICollectionViewCell,PhotoCellConfiguration {

    @IBOutlet weak var photo: UIImageView!
    private var photoURL: URL?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(with url: URL?) {
        photo.image = UIImage.init(named: "placeholder")
        photoURL = url
        if let url = url {
            ImageDownloader.sharedInstance.fetch(url: url) { [weak self] (image) in
                if self?.photoURL == url {
                    self?.photo.image = image
                }
            }
        }
    }
}
