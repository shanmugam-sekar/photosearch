//
//  PhotosSearchViewModel.swift
//  PhotoSearch
//
//  Created by Shanmugam Sekar on 15/11/18.
//  Copyright © 2018 Shanmugam Sekar. All rights reserved.
//

import Foundation

enum FETCHMODE: Int {
    case INITIALFETCH = 1
    case BOTTOMFETCH
    case MANUALFETCH
}

protocol PhotoSearchDataSource {
    var query: String {get set}
    func getPhotoModel(at index: Int) -> PhotoModel?
    func getPhotosCount() -> Int
    func canFetchList() -> Bool
    func fetchPhotosList(for query: String, fetchMode: FETCHMODE, successHandler: @escaping () -> Void , errorHandler: @escaping (String?) -> Void)
}

class PhotosSearchViewModel: PhotoSearchDataSource {
    static let searchPlaceHolder = "Explore your favourites"
    static let photosListCellIdentifier = "photoCell"
    
    var photosList: [PhotoModel] = []
    var photoListMetaData: PhotosListMetaData?
    var page = 0
    var perPageCount = 50
    var query: String = "random"
    var isLoading: Bool = false
    
    func getPhotosCount() -> Int {        
        return photosList.count
    }
    
    func canFetchList() -> Bool {
        return !isLoading
    }
    
    func getPhotoModel(at index: Int) -> PhotoModel? {
        return photosList.count > index ? photosList[index] : nil
    }
    
    func fetchPhotosList(for query: String, fetchMode: FETCHMODE, successHandler: @escaping () -> Void , errorHandler: @escaping (String?) -> Void) {
        switch fetchMode {
        case .INITIALFETCH:
            page = 1
        case .MANUALFETCH:
            page = 1
        default:
            page = page + 1
        }
        isLoading = true
        PhotoSearchDataManager.sharedDataManager.fetchPhotosList(of: query, page: page, perPageCount: perPageCount) { [weak self] (data, error) in
            if let error = error {
                errorHandler(error)
            } else {
                if let result = data, let this = self {
                    if fetchMode == .INITIALFETCH || fetchMode == .MANUALFETCH {
                        this.photosList = result.0
                    } else if fetchMode == .BOTTOMFETCH {
                        this.photosList.append(contentsOf: result.0)
                    }
                    this.photoListMetaData = result.1
                    successHandler()
                }
            }
            self?.isLoading = false
        }
//        PhotosSearchAPI.searchAPI.fetchPhotosList(of: query, page: page, perPageCount: perPageCount) { [weak self] (data, error) in
//            if let error = error {
//                errorHandler(error)
//            } else {
//                if let result = data, let this = self {
//                    if fetchMode == .INITIALFETCH || fetchMode == .MANUALFETCH {
//                        this.photosList = result.0
//                    } else if fetchMode == .BOTTOMFETCH {
//                        this.photosList.append(contentsOf: result.0)
//                    }
//                    this.photoListMetaData = result.1
//                    successHandler()
//                }
//            }
//            self?.isLoading = false
//        }
    }
}
