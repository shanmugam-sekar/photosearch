//
//  ViewController.swift
//  PhotoSearch
//
//  Created by Shanmugam Sekar on 15/11/18.
//  Copyright © 2018 Shanmugam Sekar. All rights reserved.
//

import UIKit

class PhotosSearchController: UIViewController {

    var photosSearchViewModel: PhotoSearchDataSource = PhotosSearchViewModel()
    private var loader: UIActivityIndicatorView!
    private var collectionView: UICollectionView!
    private var photoSize: CGSize {
        let viewWidth = UIScreen.main.bounds.size.width
        let photoWidth = (viewWidth/3) - 10
        return CGSize(width: photoWidth, height: photoWidth)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addSearchBar()
        addCollectionView()
        addActivityIndicatorView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchPhotosList(query: photosSearchViewModel.query, mode: .INITIALFETCH)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    private func addSearchBar() {
        if let navigationController = navigationController {
            let frame = CGRect(origin: .zero, size: navigationController.navigationBar.frame.size)
            let searchBar = UISearchBar(frame: frame)
            searchBar.delegate = self
            searchBar.placeholder = PhotosSearchViewModel.searchPlaceHolder
            searchBar.returnKeyType = .search
            
            navigationItem.titleView = searchBar
        }
    }
    
    private func collectionViewLayout() -> UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout.init()
        layout.itemSize = photoSize
        return layout
    }
    
    private func addCollectionView() {
        let photosCollectionView = UICollectionView.init(frame: .zero, collectionViewLayout: collectionViewLayout())
        photosCollectionView.backgroundColor = UIColor.white
        photosCollectionView.register(UINib.init(nibName: "PhotoCell", bundle: nil), forCellWithReuseIdentifier: PhotosSearchViewModel.photosListCellIdentifier)        
        photosCollectionView.translatesAutoresizingMaskIntoConstraints = false
        photosCollectionView.dataSource = self
        photosCollectionView.delegate = self
        photosCollectionView.bounces = false
        
        view.addSubview(photosCollectionView)

        view.addConstraint(NSLayoutConstraint.init(item: photosCollectionView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: 0))
        view.addConstraint(NSLayoutConstraint.init(item: photosCollectionView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1.0, constant: 0))
        view.addConstraint(NSLayoutConstraint.init(item: photosCollectionView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 0))
        view.addConstraint(NSLayoutConstraint.init(item: photosCollectionView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottomMargin, multiplier: 1.0, constant: -40))
        
        collectionView = photosCollectionView
    }
    
    private func addActivityIndicatorView() {
        let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityIndicatorView.color = UIColor.red
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityIndicatorView)
        
        view.addConstraint(NSLayoutConstraint.init(item: activityIndicatorView, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1.0, constant: 0))
        view.addConstraint(NSLayoutConstraint.init(item: activityIndicatorView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottomMargin, multiplier: 1.0, constant: 0))
        
        loader = activityIndicatorView
    }

    private func fetchPhotosList(query: String,mode: FETCHMODE) {
        loader.startAnimating()
        photosSearchViewModel.fetchPhotosList(for: query,fetchMode: mode, successHandler: { [weak self] in
            DispatchQueue.main.async {
                if mode == .MANUALFETCH {
                    CATransaction.begin()
                    self?.collectionView.reloadData()
                    CATransaction.setCompletionBlock({ [weak self] in
                        if let this = self {
                            this.collectionView.scrollRectToVisible(CGRect(origin: .zero, size: this.photoSize), animated: false)
                        }
                    })
                    CATransaction.commit()
                } else {
                    self?.collectionView.reloadData()
                }
                self?.loader.stopAnimating()
            }
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.loader.stopAnimating()
            }
        }
    }
}

extension PhotosSearchController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        photosSearchViewModel.query = searchBar.text ?? ""
        fetchPhotosList(query: photosSearchViewModel.query, mode: .MANUALFETCH)
    }
    
}

extension PhotosSearchController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotosSearchViewModel.photosListCellIdentifier, for: indexPath)
        if let photoCell = cell as? PhotoCellConfiguration {
            photoCell.configureCell(with: photosSearchViewModel.getPhotoModel(at: indexPath.row)?.url)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photosSearchViewModel.getPhotosCount()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}

extension PhotosSearchController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if isPhotosViewReachedBottom(scrollView) && photosSearchViewModel.canFetchList() {
            fetchPhotosList(query: photosSearchViewModel.query, mode: .BOTTOMFETCH)
        }
    }
    
    private func isPhotosViewReachedBottom(_ scrollView: UIScrollView) -> Bool {                
        return round(scrollView.frame.size.height  + scrollView.contentOffset.y) >= round(scrollView.contentSize.height)
    }
}

