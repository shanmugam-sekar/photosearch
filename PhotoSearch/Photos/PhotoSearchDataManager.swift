//
//  PhotoSearchDataManager.swift
//  PhotoSearch
//
//  Created by Shanmugam Sekar on 16/11/18.
//  Copyright © 2018 Shanmugam Sekar. All rights reserved.
//

import Foundation

class PhotoSearchDataManager {
    static let sharedDataManager = PhotoSearchDataManager()
    
    func fetchPhotosList(of query: String,page: Int,perPageCount: Int, completion: @escaping (([PhotoModel],PhotosListMetaData)?,String?) -> Void) {
        PhotosSearchAPI.searchAPI.fetchPhotosList(of: query, page: page, perPageCount: perPageCount, completion: completion)
    }
}
