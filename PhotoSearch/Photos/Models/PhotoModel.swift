//
//  PhotoModel.swift
//  PhotoSearch
//
//  Created by Shanmugam Sekar on 15/11/18.
//  Copyright © 2018 Shanmugam Sekar. All rights reserved.
//

import Foundation

struct PhotoModel {
    var id: String?
    var owner: String?
    var secret: String?
    var server: String?
    var farm: Int?
    var title: String?
    var isPublic: Bool?
    var isFriend: Bool?
    var isFamily: Bool?
    var url: URL?
}

extension PhotoModel {
    init?(json: [String: Any]?) {
        guard let data = json else {
            return nil
        }
        id = data["id"] as? String
        owner = data["owner"] as? String
        secret = data["secret"] as? String
        server = data["server"] as? String
        farm = data["farm"] as? Int
        title = data["title"] as? String
        isPublic = data["ispublic"] as? Bool
        isFamily = data["isfamily"] as? Bool
        isFriend = data["isfriend"] as? Bool
        url = constructPhotoURL(model: self)
    }
    
    func constructPhotoURL(model: PhotoModel) -> URL? {
        if let farm = model.farm, let server = model.server, let secret = model.secret, let id = model.id {
            let string = "http://farm\(farm).static.flickr.com/\(server)/\(id)_\(secret).jpg"
            return URL.init(string: string)
        }
        return nil
    }
}
