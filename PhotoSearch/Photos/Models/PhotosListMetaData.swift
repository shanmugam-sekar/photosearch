//
//  PhotosListMetaData.swift
//  PhotoSearch
//
//  Created by Shanmugam Sekar on 15/11/18.
//  Copyright © 2018 Shanmugam Sekar. All rights reserved.
//

import Foundation

struct PhotosListMetaData {
    var pageCount: Int
    var imageCount: String
}
